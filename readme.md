## FV demo app

Live demo :-
http://still-wave-28930.herokuapp.com

Requirement:-

1) Install apache version Apache/2.4.33
2) Install PHP 7.1.18
3) Install MySQL
4) Install COMPOSER , https://getcomposer.org/download/ . Make sure it run globally.
5) Install Git, https://git-scm.com/downloads. Make sure it run globally.

Installation steps:-
1) clone the file from git repository by following command.
	git clone https://lex_marzki@bitbucket.org/lex_marzki/fvdemo.git
2) in the terminal , go to working folder , cd [fvdemo]
3) To regenate composer.lock, run the following command, composer dump-autoload
4) To restore the third-party plugin, run the following command, composer update
5) To migrate database, php artisan migrate
6) To seed data, php artisan db:seed
7) Run the application, php artisan serve

To test :
1) Download the test data from https://drive.google.com/open?id=1pd6sXFiAHiGJAOR0HZ0mAK7SRo4e9DZ_
2) In the local machine, go to the browser http://localhost:8000
3) For the question 1, please use the test file on step 1.
4) For the question 2, search might be slow due to the free database network limit.
5) For the question 3, you can enter any text.

Database : 

https://db4free.net/phpMyAdmin

username : lexmarzki
password : P@ssw0rd1

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)


