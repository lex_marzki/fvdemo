<?php

class Api_Customer_Controller extends \BaseController {

	public $restful = true;

	public function get_validate() 
	{
		$message = "Validated.";

		return Response::json(array(
			'error' => true,
			'message' => $message),
			 200);
	}

	public function post_validate() 
	{
		$message = "Validated";
		$status = false;
		$fullAddress = Input::get('address');
		$address1 = '';
		$address2 = '';
		$address3 = '';

		if($fullAddress == null)
		{
			$message = "Empty address";

			return Response::json(array(
				'transformed' => $status,
				'message' => $message,
				'results' => array('address1' => $address1, 'address2' => $address2, 'address3' => $address3)),
				 200);
		}

		if(strlen($fullAddress) > 30){
			
			$status = true;
			$newAddress = str_split($fullAddress,30);
			
			foreach($newAddress as $key=>$address){
				if($key >= 3)
				 break;
				else if($key == 0)
					$address1 = $address;
				else if($key == 1)
					$address2 = $address;
				else if($key == 2)
					$address3 = $address;
			}
		}else {
			$status = false;
			$address1 = $fullAddress;
		}

		return Response::json(array(
			'transformed' => $status,
			'message' => $message,
			'results' => array('address1' => trim($address1), 'address2' => trim($address2), 'address3' => trim($address3))),
			 200);
	}


}
