<?php

class VoucherController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return View::make('voucher.index');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{	
		$fileArray = array();
		$input = Input::all();
		
		$files = Input::get('files');
		$type = Input::get('type');
		$date = date('d-m-Y',strtotime(Input::get('date')));


		if($files != null){
			foreach ($files as &$file) {
				$fileInfo = new FileVoucher();

				$this->extractNameAndType($file,$fileInfo);
			
				$this->extractDate($file,$fileInfo);
			
				if($fileInfo->fileName != null){
					array_push($fileArray,$fileInfo);
				}
				
			}
		}
		
		$result = json_encode($this->filter($type,$date,$fileArray));

		return View::make('voucher.store',compact('result'));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	private function extractNameAndType($txt,&$file){
     
		$re = '/^(.*?)(H|D)/';

		if (strpos($txt, '.') === 0)
			return;
		
		if(preg_match($re, $txt, $matches, PREG_OFFSET_CAPTURE, 0))
		{
			$file->fileName = $matches[1][0];
	
			if($matches[2][0] == 'H')
			$file->fileType = 'Header';
			else if($matches[2][0] == 'D')
			$file->fileType = 'Detail';
		}
  
	}

	private function filter($type,$date,$files){
		 $new_array = array_filter($files, function($obj) use ($type,$date){
			if (isset($obj->fileType) && $obj->fileType == $type && $obj->fileDate == $date) {
				return true;
			}
			return false;
		});

		return $new_array;
	}
  
	private function extractDate($txt,&$file){
	
	  $re1='((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229)';	# YYYYMMDD 1
	  
	  if ($c=preg_match_all ("/".$re1."/is", $txt, $matches))
	  {
		  $yyyymmdd1=$matches[1][0];
		
		  $time = strtotime('20'. $yyyymmdd1);
  
		  $file->fileDate = date("d-m-Y", $time);     
	  }
	}


}
