<?php
use Faker\Factory as Faker;

class DriverTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		$faker = Faker::create();
		Driver::create(array(
            'name'     => 'John Doe',
            'phone_number' => '123456',
            'email' => 'john.isgreat@gmail.com'
		))->save();
		
		Driver::create(array(
			'name'     => 'Mike Ang',
            'phone_number' => '567890',
            'email' => 'micheal.red@outlook.com'
		))->save();

		Driver::create(array(
			'name'     => 'Junid',
            'phone_number' => '345345',
            'email' => 'junid.isgreat@gmail.com'
		))->save();

		foreach(range(1, 5) as $index) {
			Driver::create([
			    'name'     => $faker->name,
			    'phone_number' => $faker->phoneNumber,
			    'email' => $faker->userName . '@fvtaxi.com' 
			]);
		  }

		  foreach(range(1, 5) as $index) {
			Driver::create([
			    'name'     => $faker->name,
			    'phone_number' => $faker->phoneNumber,
			    'email' => $faker->userName . '@fvdriver.com' 
			]);
		  }

	}

}
