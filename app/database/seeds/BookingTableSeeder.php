<?php
use Faker\Factory as Faker;

class BookingTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
        $faker = Faker::create();
		Booking::create(array(
            'created_at_local'  => '2016-07-28 18:56:44',
            'driver_id' => 1,
            'passenger_id' => 1,
            'state' => 'COMPLETED',
            'country_id' => 1,
            'fare' => 26.5
		))->save();
		
        Booking::create(array(
            'created_at_local'  => '2016-09-05 03:59:33',
            'driver_id' => 2,
            'passenger_id' => 2,
            'state' => 'CANCELLED_PASSENGER',
            'country_id' => 2,
            'fare' => 100
		))->save();

        Booking::create(array(
            'created_at_local'  => '2016-06-20 19:05:56',
            'driver_id' => 3,
            'passenger_id' => 3,
            'state' => 'CANCELLED_DRIVER',
            'country_id' => 3,
            'fare' => 50
        ))->save();
        
        foreach(range(1, 10) as $index) {
            Booking::create(array(
                'created_at_local'  => '2016-06-20 19:05:56',
                'driver_id' => $faker->numberBetween(1,13),
                'passenger_id' => $faker->numberBetween(1,13),
                'state' =>  $index % 2 == 0 ? 'COMPLETED' : 'CANCELLED_DRIVER',
                'country_id' => $faker->numberBetween(1,3),
                'fare' => $faker->randomFloat(2,10,20)
            ))->save();
        }
	}

}
