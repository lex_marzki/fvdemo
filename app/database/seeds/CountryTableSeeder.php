<?php

class CountryTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating

		Country::create(array(
			'name'     => 'Malaysia'
		))->save();
		
		Country::create(array(
			'name'     => 'Singapore'
		))->save();

		Country::create(array(
			'name'     => 'Indonesia'
		))->save();


	}

}
