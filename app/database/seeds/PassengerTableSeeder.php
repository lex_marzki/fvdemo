<?php

use Faker\Factory as Faker;

class PassengerTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating

		Passenger::create(array(
			'name'     => 'Jamal'
		))->save();
		
		Passenger::create(array(
			'name'     => 'Yunos'
		))->save();

		Passenger::create(array(
			'name'     => 'Ismail'
		))->save();

		$faker = Faker::create();

        foreach(range(1, 10) as $index) {
            Passenger::create([
                'name'     => $faker->name
            ]);
        }
	}

}
