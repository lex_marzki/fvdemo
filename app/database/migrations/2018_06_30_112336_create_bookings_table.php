<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bookings', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('created_at_local')->nullable();
			$table->integer('driver_id')->unsigned()->index();
			$table->integer('passenger_id')->unsigned()->index();
			$table->string('state')->nullable();
			$table->integer('country_id')->unsigned()->index();
			$table->double('fare')->nullable();

			$table->foreign('driver_id')->references('id')->on('drivers');
			$table->foreign('passenger_id')->references('id')->on('passengers');
			$table->foreign('country_id')->references('id')->on('countries');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bookings');
	}

}
