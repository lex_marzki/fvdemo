<?php

class Passenger extends \Eloquent {
	protected $fillable = [];
	protected $table = 'passengers';

	public function bookings()
    {
        return $this->hasMany('Booking');
    }
}