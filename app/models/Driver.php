<?php

class Driver extends \Eloquent {
	protected $fillable = [];
	protected $table = 'drivers';

	public function bookings() {
        return $this->hasMany('Booking');
	}
	
	public function totalCancelled() {
		$results = $this->bookings()->where('state', 'CANCELLED_PASSENGER')->get();
		
		if($results != null)
			return $results->count();
		else
			return 0;
	}

	public function totalCompleted() {
		$results = $this->bookings()->where('state', 'COMPLETED')->get();
		
		if($results != null)
			return $results->count();
		else
			return 0;
	}

	public function passengerCountRelation()
    {
        return $this->bookings()->where('state', 'COMPLETED')->selectRaw('passenger_id, count(*) as count')->groupBy('passenger_id');
	}
	
	public function getPassengerCountAttribute(){
		return $this->passengerCountRelation->first() != null ? $this->passengerCountRelation->first()->count : 0;
   }
   
   public function totalFare() {
	 return $this->bookings()->sum('fare');
	}
 
	public function totalCommission() {
		return $this->totalFare() * 0.2;
	}
	
	public function scopeFindEmail($query,$email){
		return $query->select('*',DB::raw('count(\'bookings.passenger_id\') as passenger_count'))
					->join('bookings','drivers.id', '=', 'bookings.driver_id')
					->orderBy('passenger_count', 'desc')
					->groupBy('bookings.driver_id')
					->where('email', 'LIKE', '%'.$email.'%');
	}
	
}