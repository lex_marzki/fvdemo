<?php

class Booking extends \Eloquent {
	protected $fillable = [];
	protected $table = 'bookings';

	public function driver() {
        return $this->belongsToMany('Driver');
	}
	
	public function passenger() {
        return $this->belongsTo('Passenger');
	}
}