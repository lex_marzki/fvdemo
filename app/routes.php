<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('voucher.index');
});

Route::post('driver/search', array('as'=>'driver.search', 'uses'=>'DriverController@doSearch'));


Route::resource('voucher', 'VoucherController');
Route::resource('driver', 'DriverController');
Route::resource('customer', 'CustomerController');

Route::group(array('prefix' => 'api/v1'), function () {

	Route::get('/customer/address/validate',['before' => 'api', 'uses'=>'Api_Customer_Controller@get_validate']);
	Route::post('/customer/address/validate',['before' => 'api', 'uses'=>'Api_Customer_Controller@post_validate']);
});