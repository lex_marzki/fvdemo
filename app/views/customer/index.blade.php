@extends('layouts.default')

@section('main')


<div class="row" style="margin-bottom: 20px;">
    <div class="col-sm-6">
    <h1>Customer Info</h1>
    <form id="formCustomer" >
      <div class="form-group">
        <label for="address">Address:</label>
        <input type="text" class="form-control" name="address" value="Business Office, Malcolm Long 92911 Proin Road Lake Charles Maine" />
      </div>
      <button id="btnSave" type="button" class="btn btn-primary">Process</button>
    </form>
  </div>
</div>

<div class="container" style="margin-bottom:20px">
  <h3> Transformed </h3>
  <div class="row">
      <div class="col-sm-3" >
        Address 1 : 
      </div>
      <div id="address1" class="col-sm-3"></div>
  </div>
  <div class="row">
      <div class="col-sm-3" >
        Address 2 : 
      </div>
      <div id="address2" class="col-sm-3"></div>
  </div>
  <div class="row">
      <div class="col-sm-3" >
        Address 3 : 
      </div>
      <div id="address3" class="col-sm-3"></div>
  </div>
</div>

<div class="container">
  <h3> Payload </h3>
  <div class="row">
      <div class="col-sm-6" >
  <textarea id="payload" style="width:100%;" rows="6" cols="50"></textarea>
  </div>
  </div>
</div>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@section('footer')

<script type="text/javascript">
  $(document).ready(function(){

      $("#btnSave").click(function(){

        $.post( "{{ URL::to('api/v1/customer/address/validate')}}", $("#formCustomer").serialize())
        .done(function(data) {

          $("#address1").html(data.results.address1);
          $("#address2").html(data.results.address2);
          $("#address3").html(data.results.address3);
          $("#payload").val(JSON.stringify(data));
          console.log(data);
        });


      });
  });
</script>
@stop
@stop