@extends('layouts.default')

@section('main')


<div class="container">
<h1>Result</h1>

  <div class="row voffset2">
    <div class="col-lg-12">
      <p>
      {{ $result }}
      </p>
    </div>
  </div>
  <div class="row voffset2">
        <a class="btn btn-default" href="{{ URL::previous() }}">Back</a>
  </div>
</div>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop