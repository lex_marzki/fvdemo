@extends('layouts.default')

@section('main')

<div class="container">
<div class="row">
<div class="col-sm-6">
    <h1>Voucher</h1>
</div>
</div>
<div class="row">
<div class="col-sm-6">
{{ Form::open(array('route' => 'voucher.store')) }}
  <div class="form-group">
    <label for="location">Folder location:</label>
    <input type="file" id="file" name="files[]" class="form-control" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" multiple >
  </div>
  <div class="form-group">
    <label for="type">Select Type:</label>
    <select id="type" name="type" class="form-control" >
        <option value="Header">Header</option>
        <option value="Detail">Detail</option>
    </select>
  </div>
  <div class="form-group">
    <label for="date">Date:</label>
    <input type="date" name="date" id="date" class="form-control" value="2017-09-22" />
  </div>
  <button type="submit" class="btn btn-primary">Process</button>
</form>
{{ Form::close() }}
</div>
</div>
</div>
@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop