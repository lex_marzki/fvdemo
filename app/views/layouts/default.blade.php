<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	
        <style>
            table form { margin-bottom: 0; }
           
            .error { color: red; font-style: italic; }

            .navbar-nav li.active > a {
                color: #FFFF !important;     
                background-color: #333;     
            }
        </style>

    </head>

    <body>
    <div id="topheader">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="./">FV Demo</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            <li {{ Request::is( 'voucher') ? 'class="nav-item active"' : 'class="nav-item"' }} >
                <a class="nav-link" href="{{ URL::to('voucher') }}">Question 1</a>
            </li>
            <li {{ Request::is('driver') ? 'class="nav-item active"' : 'class="nav-item"' }} >
                <a class="nav-link" href="{{ URL::to('driver') }}">Question 2</a>
            </li>
            </li>
            <li  {{ Request::is('customer') ? 'class="nav-item active"' : 'class="nav-item"' }}>
                <a class="nav-link" href="{{ URL::to('customer') }}">Question 3</a>
            </li>
            </ul>
        </div>
        </nav>
        </div>
        <div class="container" style="margin-top:20px;">
            @if (Session::has('message'))
                <div class="flash alert">
                    <p>{{ Session::get('message') }}</p>
                </div>
            @endif

            @yield('main')
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	    <!-- Latest compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
        <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

  
        @yield('footer')
    </body>

</html>