@extends('layouts.default')

@section('main')

<div class="container">
<div class="row">
<div class="col-sm-6">
    <h1>Search Driver</h1>
</div>
</div>
<div class="row">
<div class="col-sm-6">
{{ Form::open(array('route' => 'driver.search')) }}
  <div class="form-group">
    <label for="search">Email:</label>
    <input type="text" class="form-control" name="email" value="" />
  </div>
  <div class="form-group">
    <label for="type">Expected Column:</label>
        <ul type="disc">
            <li>Number of completed ride</li>
            <li>Number of cancelled ride</li>
            <li>Total completed passenger</li>
            <li>Total completed fare</li>
            <li>Total FV commission</li>
        </ul>
  </div>

  <button type="submit" class="btn btn-primary">Search</button>
{{ Form::close() }}
</div>
</div>
</div>
@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop