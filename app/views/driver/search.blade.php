@extends('layouts.default')

@section('main')


<h1>Result</h1>


   <div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                <label for="filter1">Total Completed: (more than) </label>
                <input type="text" class="form-control" id="filter1" name="filter1" value="" />
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <label for="filter2">Passenger: (less than) </label>
                <input type="text" class="form-control" id="filter2" name="filter2" value="" />
            </div>
        </div>
    </div>
    </div>

    <table id="tblDriver" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Id</th>
                <th>Email</th>
		        <th>Completed Ride</th>
		        <th>Cancelled Ride</th>
                <th>Passenger</th>
                <th>Total Fare (MYR)</th>
                <th>Total Commission (MYR)</th>
		    </tr>
        </thead>

        <tbody>
        @if (isset($drivers))
            @foreach ($drivers as $driver)
                <tr>
                    <td>{{ $driver->id }}</td>
                    <td>{{ $driver->email }}</td>
                    <td>{{ $driver->totalCompleted() }}</td>
                    <td>{{ $driver->totalCancelled() }}</td>
                    <td>{{ $driver->passengerCount }}</td>
                    <td>{{ $driver->totalFare() }}</td>
                    <td>{{ $driver->totalCommission() }}</td>
                </tr>
            @endforeach
        @else
            <tr style="text-align: center;">
                <td colspan="7">No Result</td>
            </tr>
        @endif
        </tbody>
      
    </table>


<br />
  <a class="btn btn-default" href="{{ URL::previous() }}">Back</a>

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@section('footer')
<script type="text/javascript">

    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var totalCompletedFilter = parseInt( $('#filter1').val(), 10 );
            var totalPassengerFilter = parseInt( $('#filter2').val(), 10 );
            var totalCompleted = parseInt( data[2] ) || 0; 
            var totalPassenger = parseInt( data[4] ) || 0; 

             if ( ( isNaN( totalCompletedFilter ) && isNaN( totalPassengerFilter ) ) ||
             ( isNaN( totalPassengerFilter ) && totalCompleted > totalCompletedFilter ) ||
             ( totalCompleted > totalCompletedFilter  && isNaN( totalPassengerFilter ) ) ||
             ( totalCompleted > totalCompletedFilter && totalPassengerFilter < totalPassenger ) 
             )
            {
                return true;
            }
                
            return false;
        }
    );

    $(document).ready( function () {
        var table = $('#tblDriver').DataTable({
        /* No ordering applied by DataTables during initialisation */
            "order": []
        });

        $('#filter1,#filter2').keyup(function() {
            table.draw();
        });
    } );


</script>
@stop
@stop